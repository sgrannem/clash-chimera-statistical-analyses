# Statistical analysis of CLASH chimeras

## Contents

- [Overview](#overview)
- [Repo Contents](#repo-contents)
- [System Requirements](#system-requirements)
- [Installation Guide](#installation-guide)
- [Results](#results)
- [License](./LICENSE.txt)
- [Citation](#citation)

## Overview
This is a repository containing the scripts for identifying chimeras in CLASH data that are statistically significant.
This pipeline applies a probabilistic analysis pipeline previously used for detecting RNA-RNA interactions in human cells (Sharma et al., 2016) and adapted for the analyses of RNase E CLASH data (Waters et al., 2017). This pipeline tests the likelihood that observed chimeras could have formed spuriously. Any chimeras with p-values <= 0.05 are considered to be statisticaly significantly enriched.

## Repo Contents

- [scripts_folder](./scripts_filder): All the code for processing the data.
- [test_data](./test_data): Folder with test data used by the pipeline as input files.
- [test_code](./test_code): Folder with a jupyter notebook that you can run from within the repository to test the pipeline on you computer.
- [genome_files](./genome_files): Folder with all the Staphyloccoccus areus genome files that we use for the chimera analyses.

The notebook in the test_code directory provides examples of how to run the code so that you can use it for your own data.

## System Requirements

### Hardware Requirements

RAM: 16+ GB  
CPU: At least four cores are recommended.

The runtimes vary considerably, depending on how large the sequencing data files are.
But a typical run generally takes several hours to complete.

### Software requirements

Linux: Ubuntu 18.04 and higher.

Other dependencies that need to be installed.

 - Python 3.6+ or higher 
 - pyCRAC 1.5.1 or higher (https://git.ecdf.ed.ac.uk/sgrannem/pycrac)
 - R 3.6 or higher (has been tested on R4.05)
 - dplyr R package

## Installation Guide

All you need to do is to make sure that the scripts in the scripts_folder are added to PATH in linux
For this you would need to add the path of this directory to the .bash_profile or .bashrc file.

## Results

To run the code on the test data, please have a look at the jupyter notebook in the test_code directory.
The results of these test runds can be found in the test_code/test_run directory.

## Citation

For usage of the package and associated manuscript, please cite the following papers:

1. Waters, S. A. et al. Small RNA interactome of pathogenic E. coli revealed through crosslinking of RNase E. EMBO J. 36, 374–387 (2017).
2. Iosub, I. A. et al. Hfq CLASH uncovers sRNA-target interaction networks linked to nutrient availability adaptation. Elife 9, 1–33 (2020).
3. McKellar, S. W. et al. RNase III CLASH in MRSA uncovers sRNA regulatory networks coupling metabolism to toxin expression. submitted (2021).
