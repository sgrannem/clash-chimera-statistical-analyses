#!/usr/bin/python

__author__		= "Sander Granneman"
__copyright__	= "Copyright 2019"
__version__		= "0.0.2"
__credits__		= ["Sander Granneman"]
__maintainer__	= "Sander Granneman"
__email__		= "sgrannem@staffmail.ed.ac.uk"
__status__		= "beta"
import sys
from optparse import OptionParser
from collections import defaultdict

def extractStartEndStrand(a,b):
	start = int(a)
	end = int(b)
	strand = "+"
	if a > b:
		strand = "-"
		start = b
		end = a
	else:
		start = a
		end = b
	return start,end,strand

def main():
	parser = OptionParser()
	parser.add_option("-f",dest="filename",help="the name of your .hyb input file",default=None)
	parser.add_option("-o",dest="outputfile",help="the name of your GTF output file. Default is the terminal",default=None)
	parser.add_option("--split",dest="split",action="store_true",help="to make a seperate GTF output file for each combination",default=False)
	parser.add_option("--join",dest="join",action="store_true",help="to join the gene names of the chimeric reads in the output",default=False)
	(options, args) = parser.parse_args()
	inputfile = sys.stdin
	if options.filename:
		inputfile = open(options.filename,"r")
	outputfile = sys.stdout
	if options.split:
		combodict = defaultdict(list)
		for line in inputfile:
			linesplit = line.rstrip().split('\t')
			genea,geneb = linesplit[-2:]
			if genea == "." or geneb == ".":
				continue
			else:
				if options.join:
					chromosome = linesplit[3]
					start,end,strand = extractStartEndStrand(linesplit[6],linesplit[7])
					combodict[(genea,geneb)].append("%s\tchimera\texon\t%s\t%s\t1\t%s\t.\tgene_name \"%s_%s\"\n" % (chromosome,start,end,strand,genea,geneb))
					chromosome = linesplit[9]
					start,end,strand = extractStartEndStrand(linesplit[12],linesplit[13])
					combodict[(genea,geneb)].append("%s\tchimera\texon\t%s\t%s\t1\t%s\t.\tgene_name \"%s_%s\"\n" % (chromosome,start,end,strand,geneb,genea))
				else:
					chromosome = linesplit[3]
					start,end,strand = extractStartEndStrand(linesplit[6],linesplit[7])
					combodict[(genea,geneb)].append("%s\tchimera\texon\t%s\t%s\t1\t%s\t.\tgene_name \"%s\"\n" % (chromosome,start,end,strand,genea))
					chromosome = linesplit[9]
					start,end,strand = extractStartEndStrand(linesplit[12],linesplit[13])
					combodict[(genea,geneb)].append("%s\tchimera\texon\t%s\t%s\t1\t%s\t.\tgene_name \"%s\"\n" % (chromosome,start,end,strand,geneb))
				
		if not options.outputfile:
			outfile = "chimeras.gtf"
		else:
			outfile = options.outputfile
		for key in combodict:
			outputfile = open("%s_%s" % ("_".join(key),outfile),"w")
			outputfile.write("%s" % "".join(combodict[key]))
	else:
		if options.outputfile:
			outputfile=open(options.outputfile,"w")
		for line in inputfile:
			#### extract strand, chromosomal start and end ####
			linesplit = line.rstrip().split('\t')
			genea,geneb = linesplit[-2:]
			if genea == "." or geneb == ".":
				continue
			else:
				#print chromosome,start,end,strand,gene_name
				if options.join:
					chromosome = linesplit[3]
					start,end,strand = extractStartEndStrand(linesplit[6],linesplit[7])
					outputfile.write("%s\tchimera\texon\t%s\t%s\t1\t%s\t.\tgene_name \"%s_%s\"\n" % (chromosome,start,end,strand,genea,geneb))
					chromosome = linesplit[9]
					start,end,strand = extractStartEndStrand(linesplit[12],linesplit[13])
					outputfile.write("%s\tchimera\texon\t%s\t%s\t1\t%s\t.\tgene_name \"%s_%s\"\n" % (chromosome,start,end,strand,geneb,genea))
				else:
					chromosome = linesplit[3]
					start,end,strand = extractStartEndStrand(linesplit[6],linesplit[7])
					outputfile.write("%s\tchimera\texon\t%s\t%s\t1\t%s\t.\tgene_name \"%s\"\n" % (chromosome,start,end,strand,genea))
					chromosome = linesplit[9]
					start,end,strand = extractStartEndStrand(linesplit[12],linesplit[13])
					outputfile.write("%s\tchimera\texon\t%s\t%s\t1\t%s\t.\tgene_name \"%s\"\n" % (chromosome,start,end,strand,geneb))		
		outputfile.close()
	
if __name__ == "__main__":
	main()