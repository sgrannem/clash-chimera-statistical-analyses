#!/usr/bin/python
# annotate_hyb_3.py
#
# copyright 2019 Sander Granneman
# GNU Lesser General Public License
#
#

import sys
import os
import pandas as pd
from pyCRAC.Parsers import GTF2
from pyCRAC.Methods import numpy_overlap, reverse_strand
from collections import defaultdict
from optparse import OptionParser


__version__ = 0.2

parser = OptionParser()
parser.add_option("-f","--input_file",dest="input_file",help="the path and name of our hyb input file",default=None)
parser.add_option("-o","--output_file",dest="output_file",help="provide the path to the output file name",default=None)
parser.add_option("-g", "--gtf", dest="annot_gtf", help="gtf to use for hyb annotation",default=None)
(options, args) = parser.parse_args()

assert options.input_file, "No hyb input file provided! Please fix!\n"
assert options.output_file, "No output file name for the annotated hyb file provided. Please fix!\n"
assert options.annot_gtf, "No gtf annotation file provided. Please fix!\n"

def annotateHyb(inputfile,outputfile,gtf_file):   
    ### setting the input and output streams

    datain = open(inputfile,"r")
    dataout = open(outputfile,"w")
    
    ###

    ### Making GTF2 parser object and parsing GTF annotation file ###
    
    gtf = GTF2.Parse_GTF()
    gtf.read_GTF(gtf_file,transcripts=False)
    
    ###

    list_of_tuples = defaultdict(list)

    for line in datain:
        Fld = line.strip().split("\t")

        ### Processing the first coordinates
        strand = str()
        chromosome = Fld[3].split("|")[0] 		# to make it compatible with Jai's hyb scripts
        start = int()
        end = int()
        first = int(Fld[6])
        second = int(Fld[7])
        if first < second: 	# strand should then be '+'
            strand = "+"
            start = first
            end = second
        else:
            strand = "-"
            start = second
            end = first

        sense_hits = list()
        anti_sense_hits = list()
        annotations = list()

        try:
            if (chromosome,strand) not in list_of_tuples:
                list_of_tuples[(chromosome,strand)] = gtf.chromosomeGeneCoordIterator(chromosome,numpy=True,strand=strand)
            if len(list_of_tuples[(chromosome,strand)]) > 0:
                sense_hits = numpy_overlap(list_of_tuples[(chromosome,strand)],start,end-1)
        except AssertionError:
            pass

        if sense_hits:
            resultstring = ",".join(sense_hits)
            try:
                for i in sense_hits:
                    annotation = list(gtf.annotations(i))[0]
                    annotations.append(annotation)
            except TypeError:
                print(i)
                break
        if not sense_hits:  ### Looking for anti-sense hits to gene features, including intergenic regions.
            try:
                strand = reverse_strand(strand)
                if (chromosome,strand) not in list_of_tuples:
                    list_of_tuples[(chromosome,strand)] = gtf.chromosomeGeneCoordIterator(chromosome,numpy=True,strand=strand)
                if len(list_of_tuples[(chromosome,strand)]) > 0:
                    anti_sense_hits = numpy_overlap(list_of_tuples[(chromosome,strand)],start,end-1)
            except AssertionError:
                pass
        if anti_sense_hits:
            for i in anti_sense_hits:
                annotation = list(gtf.annotations(i))[0]
                annotations.append("as_%s" % annotation)
            resultstring = ",".join(["as_%s" % i for i in anti_sense_hits])
        if not sense_hits and not anti_sense_hits:
            sys.stdout.write("Can't find hits for %s %s %s %s\n" % (chromosome,start,end,strand))
            resultstring = "unknown"
            annotations = ["unknown"]

        annotations = ",".join(annotations)
        
        Fld[3] = "|".join([chromosome,resultstring,annotations])
        
        ###

        ### Processing the second coordinates

        strand = str()
        chromosome = Fld[9].split("|")[0] 		# to make it compatible with Jai's hyb scripts
        start = int()
        end = int()
        first = int(Fld[12])
        second = int(Fld[13])
        if first < second: 	# strand should then be '+'
            strand = "+"
            start = first
            end = second
        else:
            strand = "-"
            start = second
            end = first

        sense_hits = list()
        anti_sense_hits = list()
        annotations = list()

        try:
            if (chromosome,strand) not in list_of_tuples:
                list_of_tuples[(chromosome,strand)] = gtf.chromosomeGeneCoordIterator(chromosome,numpy=True,strand=strand)
            if len(list_of_tuples[(chromosome,strand)]) > 0:
                sense_hits = numpy_overlap(list_of_tuples[(chromosome,strand)],start,end-1)
        except AssertionError:
            pass

        if sense_hits:
            resultstring = ",".join(sense_hits)
            try:
                for i in sense_hits:
                    annotation = list(gtf.annotations(i))[0]
                    annotations.append(annotation)
            except TypeError:
                print(i)
                break
        if not sense_hits:  ### Looking for anti-sense hits to gene features, including intergenic regions.
            try:
                strand = reverse_strand(strand)
                if (chromosome,strand) not in list_of_tuples:
                    list_of_tuples[(chromosome,strand)] = gtf.chromosomeGeneCoordIterator(chromosome,numpy=True,strand=strand)
                if len(list_of_tuples[(chromosome,strand)]) > 0:
                    anti_sense_hits = numpy_overlap(list_of_tuples[(chromosome,strand)],start,end-1)
            except AssertionError:
                pass
        if anti_sense_hits:
            for i in anti_sense_hits:
                annotation = list(gtf.annotations(i))[0]
                annotations.append("as_%s" % annotation)
            resultstring = ",".join(["as_%s" % i for i in anti_sense_hits])
        if not sense_hits and not anti_sense_hits:
            sys.stdout.write("Can't find hits for %s %s %s %s\n" % (chromosome,start,end,strand))
            resultstring = "unknown"
            annotations = ["unknown"]

        annotations = ",".join(annotations)
        
        Fld[9] = "|".join([chromosome,resultstring,annotations])
        
        ###

        dataout.write("%s\n" % "\t".join(Fld))

def main():
	annotateHyb(options.input_file,options.output_file,options.annot_gtf)
        
if __name__ == "__main__":
	main()