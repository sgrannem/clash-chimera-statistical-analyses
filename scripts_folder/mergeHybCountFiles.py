#!/usr/bin/python

__author__		= "Sander Granneman"
__copyright__	= "Copyright 2019"
__version__		= "0.0.2"
__credits__		= ["Sander Granneman"]
__maintainer__	= "Sander Granneman"
__email__		= "sgrannem@staffmail.ed.ac.uk"
__status__		= "beta"

import sys
import re
import glob
from collections import defaultdict
from pyCRAC.Methods import sortbyvalue

files = glob.glob("*intermolecular_hybcounts.txt")
counter = defaultdict(int)
for filename in files:
    for line in open(filename,"r"):
        Fld = line.strip().split("\t")
        try:
            combination = tuple(sorted([Fld[1],Fld[2]]))
            counts = int(Fld[3])
            counter[combination] += counts
        except IndexError:
            continue

outfile = open("mergedHybCountData.txt","w")
srnas   = open("mergedHybCountsRNAData.txt","w")
outfile.write("Nr\tCombination\tCount\n")
srnas.write("Nr\tCombination\tCount\n")
totalcount = 1
srnascount = 1
for combination,counts in sortbyvalue(counter):
    joinedcombo = "\t".join(combination)
    outfile.write("%s\t%s\t%s\n" % (totalcount,joinedcombo,counts))
    if re.search("scRNA",joinedcombo,re.I):
        srnas.write("%s\t%s\t%s\n" % (srnascount,joinedcombo,counts))
        srnascount += 1 
    totalcount += 1
outfile.close()
srnas.close()
