#!/usr/bin/python

__author__		= "Sander Granneman"
__copyright__	= "Copyright 2019"
__version__		= "0.0.4"
__credits__		= ["Sander Granneman"]
__maintainer__	= "Sander Granneman"
__email__		= "sgrannem@staffmail.ed.ac.uk"
__status__		= "beta"

import sys
import os
from optparse import *

def main():		   
	parser = OptionParser(usage="Finds the gene names that map to your hybrids\n%prog [options] -f filename -o outfile", version="%s" % __version__)
	parser.add_option("-f", "--input_file", dest="inputfile",help="Provide the path and name of the .ua.hyb file.", metavar="FILE",default=None)
	parser.add_option("-d","--distance",dest="distance",type="int",metavar="500",help="to set the minimum distance between read coordinates in the chromosome. Default = 500",default=500)
	(options, args) = parser.parse_args()
	
	### setting the input and output streams
	datain	= sys.stdin
	if options.inputfile: 
		datain = open(options.inputfile,"r")
	filename = os.path.splitext(options.inputfile)[0]
	inter = open("%s_intermolecular.hyb" % filename,"w")
	intra = open("%s_intramolecular.hyb" % filename,"w")
	###
	
	for line in datain:
		Fld = line.strip().split("\t")
		chromosomefirst = Fld[3].split("|")[0] 		# to make it compatible with Jai's hyb files
		chromosomesecond = Fld[9].split("|")[0]		# to make it compatible with Jai's hyb files
		if chromosomefirst != chromosomesecond: 	# if the chromosome names of the hits are not the same, print to the intermolecular hybrids file
			inter.write(line)
		else:				
			### Processing the first coordinates
			strandfirst = str()
			strandsecond = str()
			startfirst = int()
			endfirst = int()
			if int(Fld[6]) < int(Fld[7]): 			# strand should then be '+'
				startfirst = int(Fld[6])
				endfirst = int(Fld[7])
				strandfirst = "+"
			else:
				startfirst = int(Fld[7])
				endfirst = int(Fld[6])
				strandfirst = "-"
				
			### Processing the second coordinates:
			startsecond = int()
			endsecond = int()
			if int(Fld[12]) < int(Fld[13]): 		# strand should then be '+'
				startsecond = int(Fld[12])
				endsecond = int(Fld[13])
				strandsecond = "+"
			else:
				startsecond = int(Fld[13])
				endsecond = int(Fld[12])
				strandsecond = "-"
			if strandfirst != strandsecond:
				inter.write(line)
			elif strandfirst == strandsecond:
				if startsecond - endfirst > options.distance:
					inter.write(line)
				elif startfirst - endsecond > options.distance:
					inter.write(line)	
				else:
					intra.write(line)
	
	inter.close()
	intra.close()
	
if __name__ == "__main__":
	main()	
				